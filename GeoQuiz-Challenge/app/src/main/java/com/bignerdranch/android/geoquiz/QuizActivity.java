package com.bignerdranch.android.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

    private static final String TAG = "QuizActivity";
    private static final String KEY_INDEX = "index";
    private static final int REQUEST_CODE_CHEAT = 0;
    private static final String KEY_ANSWERED_QUESTIONS = "questionAnsweredBank";
    private static final String KEY_ANSWERED_QUESTION_CORRECTLY = "questionAnsweredCorrectlyBank";
    private static final String KEY_CHEATED_QUESTIONS = "questionHasBeenCheatedOnBank";
    private static final String KEY_CHEAT_TOKENS = "numberCheatTokensLeft";

    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mNextButton;
    private ImageButton mPrevButton;
    private Button mCheatButton;
    private TextView mQuestionTextView;
    private TextView mCheatTokenTextView;

    private int mYOffset;

    private Question[] mQuestionBank = new Question[] {
            new Question(R.string.question_australia, true),
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas, true),
            new Question(R.string.question_asia, true),
    };
    private boolean[] mQuestionAnsweredBank = new boolean[mQuestionBank.length];
    private boolean[] mQuestionAnsweredCorrectlyBank = new boolean[mQuestionBank.length];
    private boolean[] mQuestionHasBeenCheatedOnBank = new boolean[mQuestionBank.length];

    private int mCheatTokens = 3;

    private int mCurrentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        if (savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mQuestionAnsweredBank = savedInstanceState.getBooleanArray(KEY_ANSWERED_QUESTIONS);
            mQuestionAnsweredCorrectlyBank = savedInstanceState.getBooleanArray(KEY_ANSWERED_QUESTION_CORRECTLY);
            mQuestionHasBeenCheatedOnBank = savedInstanceState.getBooleanArray(KEY_CHEATED_QUESTIONS);
            mCheatTokens = savedInstanceState.getInt(KEY_CHEAT_TOKENS);
        }

        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, r.getDisplayMetrics());
        mYOffset = Math.round(px);

        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextQuestion();
            }
        });

        mTrueButton = (Button) findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsAnswered();
                checkAnswer(true);
            }
        });

        mFalseButton = (Button) findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsAnswered();
                checkAnswer(false);
            }
        });

        mPrevButton = (ImageButton) findViewById(R.id.previous_button);
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousQuestion();
            }
        });

        mNextButton = (ImageButton) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextQuestion();
            }
        });

        mCheatButton = (Button) findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
                Intent intent = CheatActivity.newIntent(QuizActivity.this, answerIsTrue);
                startActivityForResult(intent, REQUEST_CODE_CHEAT);
            }
        });

        mCheatTokenTextView = (TextView) findViewById(R.id.cheat_tokens_text_view);
        mCheatTokenTextView.setText("Cheat tokens: " + mCheatTokens);
        if (mCheatTokens == 0 && mCheatButton.isEnabled()) {
            mCheatButton.setEnabled(false);
        }

        updateQuestion();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CODE_CHEAT) {
            if (data == null) {
                return;
            }
            if (CheatActivity.wasAnswerShown(data)) {
                mQuestionHasBeenCheatedOnBank[mCurrentIndex] = true;
                mCheatTokens--;
                mCheatTokenTextView.setText("Cheat tokens: " + mCheatTokens);
                if (mCheatTokens == 0 && mCheatButton.isEnabled()) {
                    mCheatButton.setEnabled(false);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);
        savedInstanceState.putBooleanArray(KEY_ANSWERED_QUESTIONS, mQuestionAnsweredBank);
        savedInstanceState.putBooleanArray(KEY_ANSWERED_QUESTION_CORRECTLY, mQuestionAnsweredCorrectlyBank);
        savedInstanceState.putBooleanArray(KEY_CHEATED_QUESTIONS, mQuestionHasBeenCheatedOnBank);
        savedInstanceState.putInt(KEY_CHEAT_TOKENS, mCheatTokens);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    private void nextQuestion() {
        mCurrentIndex = (mCurrentIndex +1) % mQuestionBank.length;
        updateQuestion();
    }

    private void previousQuestion() {
        mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
        if (mCurrentIndex < 0) {
            mCurrentIndex += mQuestionBank.length;
        }
        updateQuestion();
    }

    private void updateQuestion() {
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);

        mTrueButton.setEnabled(!mQuestionAnsweredBank[mCurrentIndex]);
        mFalseButton.setEnabled(!mQuestionAnsweredBank[mCurrentIndex]);
    }

    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();

        int messageResId = 0;

        if (mQuestionHasBeenCheatedOnBank[mCurrentIndex]) {
            messageResId = R.string.judgement_toast;
            // What to do? Still grant points? For now, no!
            mQuestionAnsweredCorrectlyBank[mCurrentIndex] = false;
        } else {
            if (userPressedTrue == answerIsTrue) {
                messageResId = R.string.correct_toast;
                mQuestionAnsweredCorrectlyBank[mCurrentIndex] = true;
            } else {
                messageResId = R.string.incorrect_toast;
                mQuestionAnsweredCorrectlyBank[mCurrentIndex] = false;
            }
        }

        Toast msg = Toast.makeText(this, messageResId, Toast.LENGTH_SHORT);
        msg.setGravity(Gravity.TOP, 0, mYOffset);
        msg.show();

        if (finished()) {
            showResult();
        }
    }

    private void markAsAnswered() {
        mQuestionAnsweredBank[mCurrentIndex] = true;
        updateQuestion();
    }

    private boolean finished() {
        for (int i = 0; i < mQuestionAnsweredBank.length; i++) {
            if (!mQuestionAnsweredBank[i]) {
                return false;
            }
        }
        return true;
    }

    private void showResult() {
        float correctCount = 0;
        for (int i = 0; i < mQuestionAnsweredCorrectlyBank.length; i++) {
            if (mQuestionAnsweredCorrectlyBank[i]) {
                correctCount++;
            }
        }
        float result = (correctCount / mQuestionBank.length)*100;

        Toast.makeText(this,
                "You answered " + String.valueOf(Math.round(result)) + "% correctly.",
                Toast.LENGTH_SHORT).show();
    }
}
