package com.bignerdranch.android.beatbox;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.widget.SeekBar;

public class PlaybackRateViewModel extends BaseObservable {

    public ObservableField<String> mPlaybackRateLabel;
    public ObservableInt mPlaybackRate;

    private BeatBox mBeatBox;

    public PlaybackRateViewModel(BeatBox beatBox) {
        if (beatBox != null) {
            mBeatBox = beatBox;

            mPlaybackRateLabel = new ObservableField<>();
            mPlaybackRate = new ObservableInt();

            mPlaybackRate.set(50);
            mPlaybackRateLabel.set(
                    String.format(mBeatBox.getPlaybackRateLabel(), 50)
            );
        }
    }

    public void onPlaybackRateChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mBeatBox.setPlaybackRate((progress + 50) / 100f);
        mPlaybackRateLabel.set(
                String.format(mBeatBox.getPlaybackRateLabel(), progress)
        );
    }
}
