package com.bignerdranch.android.criminalintent;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.UUID;

public class CrimePhotoDetailFragment extends DialogFragment {

    private static final String EXTRA_CRIME_ID = "CrimeId";
    private File mPhotoFile;

    public static CrimePhotoDetailFragment newInstance(Crime crime) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CRIME_ID, crime.getId());

        CrimePhotoDetailFragment fragment = new CrimePhotoDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UUID id = (UUID) getArguments().getSerializable(EXTRA_CRIME_ID);
        Crime crime = CrimeLab.get(getActivity()).getCrime(id);
        mPhotoFile = CrimeLab.get(getActivity()).getPhotoFile(crime);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bitmap bitmap = PictureUtils.getScaledBitmap(mPhotoFile.getPath(), getActivity());
        ImageView imageView = new ImageView(getActivity());
        imageView.setImageBitmap(bitmap);
        return imageView; // Probably would be way better to have a separate XML-file, but I really can't be bothered right now!
    }
}
